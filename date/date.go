package date

import "time"

const DateFormat = "2006-01-02"

type Date struct {
	time time.Time
}

// New returns date with default format (YYYY-MM-DD)
func New(d string) (dt Date, err error) {
	return NewWithFormat(d, DateFormat)
}

// NewWithFormat returns date with specified format
func NewWithFormat(d, f string) (dt Date, err error) {
	t, err := time.Parse(f, d)

	return Date{
		time: t,
	}, err
}

// String returns date string in default format (YYYY-MM-DD)
func (d Date) String() (ds string) {
	return d.StringWithFormat(DateFormat)
}

// StringWithFormat return date string in specified format
func (d Date) StringWithFormat(f string) (ds string) {
	return d.time.Format(f)
}
