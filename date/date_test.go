package date

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNew(t *testing.T) {
	r := require.New(t)

	testCases := map[string]struct {
		date     string
		hasError bool
	}{
		"valid date": {
			date: "2022-12-25",
		},
		"invalid date": {
			date:     "1111-22-33",
			hasError: true,
		},
		"empty date": {
			hasError: true,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dt, err := New(tc.date)
			if tc.hasError {
				r.Error(err, "Should have the expected error")
				return
			} else {
				r.NoError(err, "Should have no expected error")
			}

			r.Equal(tc.date, dt.String(), "Should have the expected date string")
		})
	}

}

func TestNewWithFormat(t *testing.T) {
	r := require.New(t)

	testCases := map[string]struct {
		date     string
		format   string
		hasError bool
	}{
		"valid date": {
			date:   "2022-12-25",
			format: "2006-01-02",
		},
		"valid date other format": {
			date:   "12/25/2022",
			format: "01/02/2006",
		},
		"invalid date": {
			date:     "1111-22-33",
			format:   "2006-01-02",
			hasError: true,
		},
		"invalid format": {
			date:     "2022-12-25",
			format:   "1111-22-33",
			hasError: true,
		},
		"empty date": {
			format:   "2006-01-02",
			hasError: true,
		},
		"empty format": {
			date:     "12/25/2022",
			hasError: true,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dt, err := NewWithFormat(tc.date, tc.format)
			if tc.hasError {
				r.Error(err, "Should have the expected error")
				return
			} else {
				r.NoError(err, "Should have no expected error")
			}

			r.Equal(tc.date, dt.StringWithFormat(tc.format), "Should have the expected date string")
		})
	}

}
