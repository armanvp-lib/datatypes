### Creating new date
```go
// New date with default format (YYYY-MM-DD)
d, err := datatypes.date.New("2022-12-25")
```
```go
// New date with specified format
d, err := datatypes.date.NewWithFormat("2022-12-25", "YYYY-MM-DD")
```

### Get string value in default format (YYYY-MM-DD)
```go
ds := d.String()
```

### Get string value in specified format
```go
ds := d.StringWithFormat("2006-01-02")
```